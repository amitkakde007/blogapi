# DOCKER FILE is a simple text file that contains commands a user could call to assemble an image.
# All the bellow commands would configure a local devlopment server

FROM python:3.8-slim-buster

ENV PATH="/scripts:${PATH}"

ENV PYTHONUNBUFFERED 1
COPY requirements.txt /requirements.txt
# packages required for setting up WSGI
RUN apt-get update
RUN apt-get install -y --no-install-recommends gcc libpq-dev python3-dev

RUN python3 -m pip install --upgrade pip

RUN pip install -r /requirements.txt

RUN mkdir /app
WORKDIR /app

COPY api/ /app
COPY scripts /scripts
RUN chmod +x /scripts/*

RUN mkdir -p /web/media
RUN mkdir -p /web/static

RUN adduser user
RUN chown -R user:user ../usr/local/lib/python3.8/site-packages/rest_framework_simplejwt/token_blacklist/*
RUN chown -R user:user /app
RUN chmod -R 755 /app
USER user

#CMD ["entrypoint.sh"]