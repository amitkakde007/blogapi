
from django.contrib.auth import get_user_model

from rest_framework import serializers

User = get_user_model()

class CustomUserSerializer(serializers.ModelSerializer):

    first_name = serializers.CharField(label='First Name',write_only=True)
    last_name = serializers.CharField(label='Last Name',write_only=True)
    confirm_password = serializers.CharField(label='Confirm password', write_only=True,style={'input_type':'password'})
    class Meta:
        model = User
        fields = ('first_name','last_name','fullname','email','password','confirm_password')
        extra_kwargs={
            'fullname':{
                'read_only':True
            },
            'password':{
                'write_only':True,
                'style':{
                    'input_type':'password'
                }
            }
        }


class CustomUserLoginSerializer(serializers.Serializer):

    email = serializers.CharField(label='Email ID',write_only=True)
    password = serializers.CharField(label='Password',write_only=True,style={'input_type':'password'})

class TokenSerializer(serializers.Serializer):

    auth_token = serializers.CharField()