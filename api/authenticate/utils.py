import random,threading

from django.core.mail import EmailMessage
from django.contrib.auth import get_user_model


def generate_username(name):

    first_name,last_name = name.split(' ')
    user_name = f'{(last_name[0:3]+first_name[0:2]).lower()}{random.randint(1000,9999)}'
    while True:
        if not get_user_model().objects.filter(username=user_name).exists():
            return user_name
        else:
            user_name = f'{(last_name[0:3]+first_name[0:2]).lower()}{random.randint(1000,9999)}'

class EmailThread(threading.Thread):
    
    def __init__(self,email):
        self.email = email
        threading.Thread.__init__(self)
    
    def run(self):
        self.email.send()

class AccountVerification:

    @staticmethod
    def send_mail(data):
        user = data['user']
        email_subject = 'Verify account with BlogSite'
        email_body = f"Hi {user.fullname}\n Greetings from BlogSite\n Kindly use the below mentioned link to verify your account\n {data['url']}"
        email = EmailMessage(subject=email_subject,body=email_body,to=[user.email])
        
        EmailThread(email).start()