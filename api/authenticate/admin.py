from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from rest_framework_simplejwt.token_blacklist.models import OutstandingToken
from rest_framework_simplejwt.token_blacklist.admin import OutstandingTokenAdmin

from .models import CustomUser
# Register your models here.

class UserCreationForm(forms.ModelForm):

    password1 = forms.CharField(label='Password1',widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password1',widget=forms.PasswordInput)

    class Meta:
        model = CustomUser
        fields = ('fullname','email')
    
    def clean_password2(self):
        password1 = self.changed_data('password1')
        password2 = self.changed_data('password2')
        if password1 and password2 and password1 != password2:
            raise ValidationError('Password do not match!')
        
    def save(self,commit=True):
        user = super.save(commit=False)
        user.set_password(self.cleaned_data(['password1']))
        if commit:
            user.save()
        return user

class UserChangeForm(forms.ModelForm):

    password = ReadOnlyPasswordHashField()

    class Meta:
        model = CustomUser
        fields = ('fullname','email','is_active','is_verified','is_admin')

class UserAdmin(BaseUserAdmin):

    form = UserChangeForm
    add_form = UserCreationForm

    list_display = ('username','email','is_active','is_verified','is_admin')
    list_filter = ('is_admin','is_active','is_verified')
    fieldsets = (
        (None, {'fields': ('email', 'password','is_verified')}),
        ('Personal info', {'fields': ('fullname',)}),
        ('Permissions', {'fields': ('is_admin','is_active')}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()


class CustomOutstandingTokenAdmin(OutstandingTokenAdmin):

    def has_delete_permission(self, *args, **kwargs):
        return True

admin.site.register(CustomUser,UserAdmin)
admin.site.unregister(OutstandingToken)
admin.site.register(OutstandingToken,CustomOutstandingTokenAdmin)
admin.site.unregister(Group)