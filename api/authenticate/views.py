
import json
import os, jwt

from django.urls import reverse
from django.conf import settings
from django.contrib.auth import get_user_model,authenticate
from django.contrib.sites.shortcuts import get_current_site

from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import generics, views,status, viewsets
from rest_framework.exceptions import AuthenticationFailed,ValidationError

from rest_framework_simplejwt.tokens import RefreshToken


from .google import Google
from .register import register_login
from .utils import generate_username, AccountVerification
from .serializers import CustomUserSerializer, CustomUserLoginSerializer, TokenSerializer
# Create your views here.

User = get_user_model()
class CustomUserViewSet(viewsets.ViewSet):

    queryset = User.objects.all()
    serializer_class = CustomUserSerializer

    def create(self, request):

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            fullname = f"{serializer.validated_data.get('first_name')} {serializer.validated_data.get('last_name')}"
            confirm_password = serializer.validated_data.get('confirm_password')
            if serializer.validated_data.get('password') == confirm_password:
                user = User.objects.create(
                    fullname = fullname,
                    username = generate_username(fullname),
                    email = serializer.validated_data.get('email'),
                )
                user.set_password(confirm_password)
                user.save()
                tokens = user.get_tokens()
                absolute_url = f"http://{get_current_site(request)}{reverse('email-verify')}?token={str(tokens['access'])}"
                data = {'url':absolute_url,'user':user}
                AccountVerification.send_mail(data)
                RefreshToken(tokens['refresh']).blacklist()
                return Response(data={
                    'email': user.get_email(),
                    'message': f'{user.fullname} your account has been created successfully! Please verify your account using the link that is waiting in your inbox',
                },status=status.HTTP_201_CREATED)
            else:
                raise ValidationError('Password do not match Try again!')
        else:
            return Response(data=serializer.errors,status=status.HTTP_400_BAD_REQUEST)

class CustomUserLoginView(views.APIView):

    serializer_class = CustomUserLoginSerializer

    def post(self,request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            try:
                is_user = User.objects.filter(email__exact=serializer.validated_data.get('email'))[0]
                user = authenticate(username=is_user.username,password=serializer.validated_data.get('password'))
                response_data = {
                    'fullname':user.fullname,
                    'email': user.get_email()
                    }
                tokens = user.get_tokens()
                response = Response(data=response_data,status=status.HTTP_200_OK)
                response.set_cookie('access',tokens['access'],httponly=False)
                response.set_cookie('refresh',tokens['refresh'],max_age=86400, httponly=True)
                return response
            except User.DoesNotExist as e:
                return Response(data=e,status=status.HTTP_404_NOT_FOUND)

class EmailVerificationView(generics.GenericAPIView):

    def get(self,request):

        try:
            pay_load = jwt.decode(jwt=request.GET.get('token'),key=settings.SECRET_KEY,algorithms=['HS256'])
            user = User.objects.get(id=pay_load['user_id'])
            if not user.is_verified:
                user.is_verified, user.is_active = True, True
                user.save()
                tokens = user.get_tokens()
                response = Response(data={'message':'Account verification successfull!',},status=status.HTTP_200_OK)
                response.set_cookie('access',tokens['access'],httponly=False)
                response.set_cookie('refresh',tokens['refresh'],max_age=86400, httponly=True)
                return response
        except jwt.ExpiredSignatureError:
            return Response(data={'message':'Activation expired'},status=status.HTTP_400_BAD_REQUEST)
        except jwt.DecodeError:
            return Response(data={'message':'Invalid Token'},status=status.HTTP_400_BAD_REQUEST)


class GoogleLoginView(views.APIView):

    serializer_class = TokenSerializer

    def validate_token(self,auth_token):

        user_data = Google.validate(auth_token)
        try:
            user_data['sub']
        except:
            raise ValidationError('The token is invalid or expired. Please login again')

        if user_data['aud']!= os.environ.get('GOOGLE_CLIENT_ID'):
            raise AuthenticationFailed('Sorry user not found !')
        else:
            fullname = user_data['name']
            email = user_data['email']
            auth_provider = 'Google'
            return register_login(fullname,email,auth_provider)


    def post(self,request):

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            data = self.validate_token(serializer.validated_data.get('auth_token'))
            if data:
                tokens = data['token']
                response  = Response(data={
                    'fullname':data['fullname'],
                    'email':data['email']
                    },status=status.HTTP_200_OK)
                response.set_cookie('access',tokens['access'],httponly=False)
                response.set_cookie('refresh',tokens['refresh'],max_age=86400, httponly=True)
                return response
            else:
                return Response(data=serializer.errors,status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def access_token(request):
    if request.method=='POST':
        refresh = RefreshToken(request.COOKIES['refresh'])
        response = Response(data={'message':'Success'},status=status.HTTP_201_CREATED)
        response.set_cookie('access',str(refresh.access_token))
        return response


@api_view(['GET'])
def fetch_logged_user(request):
    if request.method=='GET':
        try:
            payload = jwt.decode(jwt=request.GET.get('token'),key=settings.SECRET_KEY,algorithms=['HS256'])
            user_obj = User.objects.get(id=payload['user_id'])
            user = {'fullname':user_obj.fullname,'email': user_obj.get_email()}
            return Response(data=user, status=status.HTTP_200_OK)
        except jwt.ExpiredSignatureError:
            return Response(data={'message':'Activation expired'},status=status.HTTP_400_BAD_REQUEST)
        except jwt.DecodeError:
            return Response(data={'message':'Invalid Token'},status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response(data={'Message':e},status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def logout_user(request):
    if request.method=='POST':
        refresh_token = RefreshToken(request.COOKIES['refresh'])
        refresh_token.blacklist()
        response = Response(data={'message':'Logged out successfully'},status=status.HTTP_200_OK)
        response.delete_cookie('refresh')
        return response