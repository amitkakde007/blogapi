import os

from django.contrib.auth import get_user_model,authenticate

from .utils import generate_username

USER = get_user_model()
PASSWORD = os.environ.get('USER_SECRET_KEY')


def register_login(fullname,email,provider):

    if not USER.objects.filter(email=email).exists():
        user_data ={
            'username' :generate_username(fullname),
            'fullname':fullname,
            'email':email,
            'auth_provider':provider
        }
        new_user = USER.objects.create(**user_data)
        new_user.is_verified = True
        new_user.is_active = True
        new_user.set_password(PASSWORD)
        new_user.save()
            
        new_user_obj = authenticate(username=user_data['username'],password=PASSWORD)
        return{
            'fullname':new_user_obj.fullname,
            'email': new_user_obj.get_email(),
            'token': new_user_obj.get_tokens()
        }
    else:
        user = USER.objects.get(email=email)
        registerd_user = authenticate(username=user.username,password=PASSWORD)
        return{
            'fullname':registerd_user.fullname,
            'email': registerd_user.get_email(),
            'token': registerd_user.get_tokens()
        }