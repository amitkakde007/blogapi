from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models
from django.contrib.auth.models import BaseUserManager

# Create your models here.
from rest_framework_simplejwt.tokens import RefreshToken
class UserManager(BaseUserManager):

    def create_user(self, username, password=None):

        if not username:
            raise ValueError('User must have username')
        
        user = self.model(
            username= username,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user
    
    def create_superuser(self,username,password=None):

        user = self.create_user(
            username=username,
            password=password
        )
        user.is_admin=True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser):
    
    fullname = models.CharField(verbose_name='Full name', max_length=100,blank=True)
    username = models.CharField(verbose_name='Username',max_length=9,blank=False,unique=True)
    email = models.CharField(verbose_name='Email Id',max_length=255,unique=True)
    is_active = models.BooleanField(default=False)
    is_verified = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)

    auth_provider = models.CharField(max_length=10, default='None', blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELD = ['email']

    def __str__(self):
        return self.fullname
    
    def get_email(self):
        return self.email

    def has_perm(self,perm,obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    def get_tokens(self):
        
        refresh = RefreshToken.for_user(self)
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token),
        }
