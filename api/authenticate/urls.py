"""api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from os import name
from django.urls import path
from django.urls.conf import include

from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenObtainPairView,TokenRefreshView

from .views import CustomUserViewSet,GoogleLoginView,CustomUserLoginView,EmailVerificationView, access_token, fetch_logged_user,logout_user

router = DefaultRouter()

router.register('userviewset',CustomUserViewSet,basename='userviewset')

urlpatterns = [
    path('',include((router.urls,'user'),namespace='user'),),
    path('login/',CustomUserLoginView.as_view(),name='custom-login'),
    path('logout/',logout_user,name='user-logout'),
    path('google/login',GoogleLoginView.as_view(),name='google-login'),
    path('verify-user',fetch_logged_user,name='verify-user'),
    path('api/token/',TokenObtainPairView.as_view(),name='token-pair'),
    path('api/token/refresh/',access_token,name='access-token'),
    path('email-verify',EmailVerificationView.as_view(),name='email-verify')
    #path('api/token/refresh/1',TokenRefreshView.as_view(),name='token-refresh-default')
]
