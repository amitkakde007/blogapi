from django.db import models
from django.contrib.auth import get_user_model
from django.db.models.deletion import CASCADE

# Create your models here.

user = get_user_model()

def upload_to(instance,*args):
    return f'BlogData/{instance.author}/{instance.title}/{instance.title}_banner'

class Blog(models.Model):

    GENRE_CHOICES = [
        ('Fashion','Fashion'),
        ('Automobile','Automobile'),
        ('Science and technology','Science and technology'),
        ('Food','Food'),
        ('Travel','Travel'),
        ('Music','Music'),
        ('Fitness','Fitness'),
        ('Sports','Sports'),
        ('Finance','Finance'),
        ('Photography','Photography'),
    ]

    title = models.CharField(max_length=60,verbose_name='Blog name',blank=False,null=False)
    content = models.TextField(verbose_name='Blog content',blank=False,null=False)
    read_time= models.IntegerField(default=1,verbose_name='Time required to read the blog')
    genre = models.CharField(verbose_name='Blog Type',max_length=22,choices=GENRE_CHOICES,default='Science and technology')
    created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(user, on_delete=models.CASCADE)
    image = models.ImageField(default='untitled',upload_to=upload_to)

    def __str__(self):
        return self.title

class BlogApplaud(models.Model):

    applauded_by = models.ForeignKey(user,on_delete=models.CASCADE)
    applauded_blog = models.ForeignKey(Blog,on_delete=models.CASCADE)

    def __str__(self):
        return self.applauded_blog.title

    def get_total_applaud(self):
        return self.applauded_blog.count()

class BlogComment(models.Model):

    comment = models.CharField(verbose_name='Comment',max_length=160,blank=True,null=True)
    commented_on = models.DateTimeField(auto_now_add=True)
    comment_updated_on = models.DateTimeField(auto_now=True)
    commented_by = models.ForeignKey(user,on_delete=models.CASCADE)
    blog_comment = models.ForeignKey(Blog,on_delete=models.CASCADE)

    def __str__(self):
        return self.comment
    
    def get_commented_by(self):
        return self.commented_by.fullname
    
    def get_blog_comment(self):
        return self.blog_comment