import os
from django.test import TestCase
from django.contrib.auth import get_user_model

from blog.models import Blog

class TestBlogModel(TestCase):

    def setUp(self):

        self.user = get_user_model()
        self.password = os.environ.get('TEST_USER_CRED')
        self.user1 = {
            'fullname':'TestUser1',
            'email':'testuser@gmail.com',
        }
        
        self.blog1 = {
            'title': 'TestBlog1',
            'content': 'This is first Blog written in TestCases',
        }

        self.blog2 = {
            'title': 'TestBlog2',
            'content': 'This blog is gonna get updated',
        }

        self.blog3 = {
            'title': 'TestBlog3',
            'content': 'This blog is gonna get updated',
        }

        self.test_user1 = self.user.objects.create(**self.user1)
        self.test_user1.set_password(self.password)
        self.test_user1.save()

        self.user_obj = self.user.objects.get(pk=self.test_user1.pk)

        self.blog1['author'] = self.user_obj
        self.blog2['author'] = self.user_obj
        self.blog3['author'] = self.user_obj

    def test_create_blog(self):

        new_blog1 = Blog.objects.create(**self.blog1)
        new_blog1.save()

        self.assertEqual(new_blog1.title,self.blog1['title'])
    
    def test_list_blog(self):
        new_blog1 = Blog.objects.create(**self.blog1)
        new_blog2 = Blog.objects.create(**self.blog2)
        new_blog3 = Blog.objects.create(**self.blog3)
        
        new_blog1.save()
        new_blog2.save()
        new_blog3.save()

        self.assertCountEqual(Blog.objects.all(),[new_blog1,new_blog2,new_blog3])

    def test_update_blog(self):

        new_blog2 = Blog.objects.create(**self.blog2)
        new_blog2.save()

        new_blog2.content = 'This blog is gonna get updated now!!!'
        new_blog2.save()

        self.assertEqual(new_blog2.content,'This blog is gonna get updated now!!!')
    
    def test_delete_blog(self):

        new_blog3 = Blog.objects.create(**self.blog3)
        new_blog3.save()

        with self.assertRaises(AssertionError) as e:
            delete_note = Blog.objects.get(pk=new_blog3.id)
            delete_note.delete()
            self.assertIn(delete_note,Blog.objects.all())
        self.assertEqual(str(e.exception),'<Blog: TestBlog3> not found in <QuerySet []>')