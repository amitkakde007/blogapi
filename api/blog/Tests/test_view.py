import os
import json

from django.urls import reverse
from django.test import TestCase
from django.contrib.auth import get_user_model
from django.shortcuts import get_list_or_404, get_object_or_404

from rest_framework import status
from rest_framework.test import APIClient

from blog.models import Blog

CLIENT = APIClient()
URL = 'blog:blog-viewset'
TEST_USER_NAME = 'TestUser1'
TEST_EMAIL = 'test_user@gmail.com'
TEST_PASSWORD = os.environ.get('TEST_USER_CRED')


class TestListBlogViews(TestCase):

    def setUp(self):

        test_user = get_user_model().objects.create(fullname=TEST_USER_NAME,email=TEST_EMAIL)
        test_user.set_password(TEST_PASSWORD)
        test_user.save()

        test_user_obj = get_user_model().objects.get(id=test_user.id)

        blog1 = {
            'title':'First blogs',
            'content':'This is the first blogs',
            'author': test_user_obj
        }
        blog2 = {
            'title':'Second blogs',
            'content':'This is the second blogs',
            'author': test_user_obj
        }
        blog3 = {
            'title':'Third blogs',
            'content':'This is the third blogs',
            'author': test_user_obj
        }

        for blog in (blog1,blog2,blog3): Blog.objects.create(**blog)

    def test_blogs_list(self):

        response = CLIENT.get(reverse(f'{URL}-list'))
        blogs = Blog.objects.all()

        self.assertEqual(len(response.data),len(blogs))
        

class TestCreateBlogViews(TestCase):
    
    def setUp(self):

        test_user_data = {'fullname':TEST_USER_NAME,'email':TEST_EMAIL}
        testuser = get_user_model().objects.create(**test_user_data)
        testuser.set_password(TEST_PASSWORD)
        testuser.save()
        
        self.testuser_object = get_user_model().objects.get(pk=testuser.id)

    def test_valid_blog(self):

        valid_blog = {
            'title': 'Blog testing-1',
            'content':'This is getting created over TestCase',
            'genre':'Food',
            'author': self.testuser_object.id
        }

        response = CLIENT.post(reverse(f'{URL}-list'),
            data=valid_blog,
            content = 'application/json'
        )
        self.assertEqual(response.status_code,status.HTTP_201_CREATED)


    def test_invalid_blog(self):

        invalid_blog = {
            'title':"",
            'content': 'This is another testcase for blog ',
            'genre':"",
            'author':""
        }

        response = CLIENT.post(reverse(f'{URL}-list'),data=invalid_blog,content_type='application/json')
        
        self.assertEqual(response.status_code,status.HTTP_400_BAD_REQUEST)

class TestUpdateBlogViews(TestCase):

    def setUp(self):

        test_user_data = {'fullname':TEST_USER_NAME,'email':TEST_EMAIL}
        testuser = get_user_model().objects.create(**test_user_data)
        testuser.set_password(TEST_PASSWORD)
        testuser.save()
        
        self.testuser_object = get_user_model().objects.get(pk=testuser.id)

        self.blog_data = {
            'title': 'Blog testing-1',
            'content':'This is getting created over TestCase',
            'genre':'Food',
            'author': self.testuser_object
        }
        self.blog1 =  Blog.objects.create(**self.blog_data)

    def test_valid_blog(self):

        payload = {
            'title': 'Blog testing-1 updated',
            'content':'This is getting created over TestCase',
            'genre':'Science and technology',
            'author': self.testuser_object.id
            }

        response = CLIENT.put(reverse(f'{URL}-detail',kwargs = {'pk':self.blog1.pk},),
            data=json.dumps(payload),
            content_type='application/json'
        )
        
        blog = get_object_or_404(Blog,pk=self.blog1.id)
        self.assertEqual(response.status_code,status.HTTP_204_NO_CONTENT)
        self.assertEqual(blog.title,payload['title'])

class TestRetrieveBlogViews(TestCase):

    def setUp(self):

        test_user = {'fullname':TEST_USER_NAME,'email':TEST_EMAIL}
        test_user_obj = get_user_model().objects.create(**test_user)
        test_user_obj.set_password(TEST_PASSWORD)
        test_user_obj.save()

        test_user_obj = get_user_model().objects.get(id=test_user_obj.id)

        self.blog1_data = {
            'title':'Testing blog 1',
            'content': 'To test retrieve method',
            'genre':'Food',
            'author': test_user_obj
        }

        self.blog = Blog.objects.create(**self.blog1_data)


    def test_blog_exists(self):

        response = CLIENT.get(reverse(f'{URL}-detail',kwargs={'pk':self.blog.id}))

        blog = Blog.objects.get(id=self.blog.id)
        self.assertEqual(response.status_code,status.HTTP_200_OK)
        self.assertEqual(blog.title,self.blog1_data['title'])

    def test_blog_not_found(self):

        response = CLIENT.get(reverse(f'{URL}-detail',kwargs={'pk':30}))
        self.assertEqual(response.status_code,status.HTTP_404_NOT_FOUND)


class TestDeleteBlogViews(TestCase):

    def setUp(self):

        test_user = get_user_model().objects.create(fullname = TEST_USER_NAME,email = TEST_EMAIL)
        test_user.set_password(TEST_PASSWORD)
        test_user.save()

        test_user_obj = get_user_model().objects.get(id=test_user.id)

        blog1 = {
            'title':'First blogs',
            'content':'This is the first blogs',
            'author': test_user_obj
        }
        blog2 = {
            'title':'Second blogs',
            'content':'This is the second blogs',
            'author': test_user_obj
        }
        blog3 = {
            'title':'Third blogs',
            'content':'This is the third blogs',
            'author': test_user_obj
        }

        self.blog1_obj,self.blog2_obj,self.blog3_obj = [Blog.objects.create(**blog) for blog in (blog1,blog2,blog3)]

        self.blogs = get_list_or_404(Blog)

    def test_blog_success(self):

        response = CLIENT.delete(reverse(f'{URL}-detail',kwargs={'pk':self.blog1_obj.id}))

        self.assertEqual(response.status_code,status.HTTP_200_OK)
        self.assertNotEqual(len(response.data),len(self.blogs))
        self.assertNotIn(self.blog1_obj,response.data)
    
    def test_blog_failed(self):

        response = CLIENT.delete(reverse(f'{URL}-detail',kwargs={'pk':34}))

        self.assertEqual(response.status_code,status.HTTP_404_NOT_FOUND)
        self.assertListEqual([self.blog1_obj,self.blog2_obj,self.blog3_obj],self.blogs)