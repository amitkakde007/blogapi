
from django.contrib.auth import get_user_model

from rest_framework import serializers

from blog.models import Blog

User = get_user_model()
class BlogAuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','fullname','email')

class BlogSerializer(serializers.ModelSerializer):

    author = BlogAuthorSerializer(read_only=True)
    class Meta:
        model = Blog
        fields = ('id','title','content','genre','read_time','image','last_modified','author')
        extra_kwargs={
            'read_time':{
                'read_only':True
            }
        }