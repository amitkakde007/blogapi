# Generated by Django 3.2.4 on 2021-06-23 10:08

import blog.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_alter_blogimage_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='blog',
            name='image',
            field=models.ImageField(default='untitled', upload_to=blog.models.upload_to),
        ),
        migrations.DeleteModel(
            name='BlogImage',
        ),
    ]
