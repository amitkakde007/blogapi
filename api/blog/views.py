import math

from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404

from rest_framework.response import Response
from rest_framework import permissions, status,viewsets


from .models import Blog
from .serializers import BlogSerializer
# Create your views here.

User = get_user_model()

def get_read_time(value):

    words_count= len([val.strip() for val in value.split()])
    seconds_value,minutes = math.modf(words_count/200)
    seconds = round(seconds_value*0.60,2)
    if seconds > 0.3:
        minutes += 1
        return minutes
    elif seconds <= 0.3 and minutes >= 1:
        return int(minutes)
    elif seconds <= 0.3 and minutes <= 1:
        return seconds

class BlogViewSet(viewsets.ModelViewSet):

    serializer_class = BlogSerializer
    queryset = Blog.objects.all()
    permission_classes=(permissions.IsAuthenticatedOrReadOnly,)

    
    def create(self,request):

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            author = get_object_or_404(User,id=request.user.id)
            read_time = get_read_time(serializer.validated_data.get('content'))
            serializer.save(author=author,read_time=read_time)
            return Response(data={'result': 'Blog created successfully!!!'},status=status.HTTP_201_CREATED)
        else:
            return Response(data=serializer.errors,status=status.HTTP_400_BAD_REQUEST)
    
    # def update(self,request,pk):
        
    #     blog = get_object_or_404(Blog,pk=pk)
    #     serializer = self.serializer_class(instance=blog,data=request.data)
    #     if serializer.is_valid() and blog.author==request.user:
    #         serializer.save()
    #         return Response(data={'result':'Blog updated successfully!!!'},status=status.HTTP_204_NO_CONTENT)
    #     elif not blog.author==request.user:
    #         return Response(data={'result':'Permission denied'},status=status.HTTP_401_UNAUTHORIZED)
    #     elif serializer.errors == 'Not Found':
    #         return Response(data=serializer.errors,status=status.HTTP_404_NOT_FOUND)
    #     else:
    #         return Response(data=serializer.errors,status=status.HTTP_400_BAD_REQUEST)

    # def delete(self,request,pk):

    #     blog = get_object_or_404(Blog,pk=pk)
    #     if blog and blog.author==request.user:
    #         name = blog.title
    #         blog.delete()
    #         return Response(data={'result':f'{name} Deleted successfully!!!'},status=status.HTTP_200_OK)
    #     elif blog and not blog.author==request.user:
    #         return Response(data={'result':'Permission denied'},status=status.HTTP_401_UNAUTHORIZED)
    #     elif not blog:
    #         return Response(data={'result':'Not found'},status=status.HTTP_404_NOT_FOUND)
        