from django.contrib import admin

from .models import Blog,BlogApplaud,BlogComment

# Register your models here.
admin.site.register(Blog)
admin.site.register(BlogApplaud)
admin.site.register(BlogComment)